[![CircleCI](https://circleci.com/gh/tohbay/tranzact/tree/master.svg?style=svg&circle-token=e43c1c33a8c9db243550ff8061114d8822429dce)](https://circleci.com/gh/tohbay/tranzact/tree/master)

# tranzact

"Welcome to Tranzact API, a simple money transfer api with user authentication 👍🏼 🌍 ⚡️ 🥂 🏆"

## Technologies Used

- Nodejs: an open source server framework that allows you to run JavaScript on the server.
- Express: open source server-side framework for starting out Javascript server quickly on the fly.
- PostgreSQL database for storing user data (ElephantSQL for production and local setup for development and testing).
- Sequelize ORM for interacting with the database.
- Prettier for code formatting.
- Mocha and Supertest for Testing Driven Development (TDD).
- CircleCI for Continuous Integration & Continuous Deployment (CICD).
- Travis for code quality.
- Coveralls for code coverage.
- Hosting with Heroku

## Live URL

https://apptranzact.herokuapp.com/

## API endpoints

| HTTP VERB | ENDPOINT                                        | FUNCTIONALITY                                     |
| --------- | ------------------------------------------------| --------------------------------------------------|
| POST      | api/v1/users/auth/signin                        | Signup a user account                             |
| POST      | api/v1/users/auth/signup                        | Signin a user                                     |
| POST      | api/v1/users/auth/signout                       | Signout a user                                    |
| POST      | api/v1/accounts                                 | Create a bank account                             |
| PUT       | api/v1/transactions/credit                      | Credit a specific account number                  |
| PUT       | api/v1/transactions/debit                       | Debit a specific account number                   |
| GET       | api/v1/transactions/:accountNumber/transactions | Get all transactions on a specific account number |

## Upcoming API endpoints in order of priority

| HTTP VERB | ENDPOINT                                        | FUNCTIONALITY                                     |
| --------- | ----------------------------------------------- | ------------------------------------------------- |
| GET       | api/v1/users                                    | Fetch all user accounts                           |
| GET       | api/v1/users/user/:email                        | Fetch a specific user accounts                    |
| PATCH     | api/v1/accounts/:accountNumber                  | Activate or Deactivate a bank account             |
| GET       | api/v1/accounts                                 | Fetch all bank accounts                           |
| GET       | api/v1/accounts/:accountNumber                  | Fetch a specific bank accounts                    |
| DELETE    | api/v1/accounts/:accountNumber                  | Delete a specific bank accounts                   |
| GET       | api/v1/accounts/status/dormant                  | Fetch all dormant bank accounts                   |
| GET       | api/v1/accounts/status/active                   | Fetch all active bank accounts                    |
| GET       | api/v1/accounts/user/:email                     | Fetch all bank accounts owned by a specific user  |
| GET       | api/v1/transactions/                            | Fetch all transactions                            |
| GET       | api/v1/transactions/:id                         | Fetch a specific transaction                      |
| PATCH     | api/v1/users/:email/cashier                     | Update client type from client to cashier         |
| PATCH     | api/v1/users/:email/admin                       | Update client type from cashier to admin          |
| DELETE    | api/v1/users/user/:email                        | Delete a particular user account                  |

## Guide

### API Specifications

The API endpoints should respond with a JSON object specifying the HTTP **_status_** code, custom response message **_message_** code, and either a **_data_** property (on success) or an **_error_** property (on failure).

```javascript
// Sucess
{
  "status": true,
  "message": {...}
  "data": {...}
}

// Failure
{
  "error": true,
  "message": {...}
}
```

### Data Specifications

These specifications are only a guide to aid in developing the application. You have the freedom to come up with your own specifications, as long as the API functions properly and returns appropriate responses.

```javascript
// user
{
  "id": UUIDV4,
  "username": STRING,  // any string e.g:  testing
  "email": STRING,     // any string e.g:  testing
  "passowrd": STRING,  // any string e.g:  testing
}

// account
{
  "id": UUIDV4,
  "accountNumber": UUIDV4,   // retrieved from logged in user token
  "accountType": STRING,     // saving, current, fixed-deposit
  "status": STRING,          // dormant, active
  "balance": FLOAT,          // e.g: 100.00
  "userId": UUIDV4,
}

// transaction
{
  "id": UUIDV4,
  "amount": FLOAT,            // e.g: 100.00
  "userId": UUIDV4,           // retrieved from logged in user token
  "transactionType": STRING,  // credit, debit
  "previousBalannce": FLOAT,
  "currentBalance": FLOAT,
  "accountNumberId": UUIDV4
}
```

### Endpoint Specifications

#### User Signup

```javascript

POST /api/v1/users/auth/signup

- Input
{
 "username": STRING,    // any string e.g:  testing
 "email": STRING,       // any string e.g:  testing
 "passowrd": STRING,    // any string e.g:  testing
}
```

#### User Signin

```javascript

POST /api/v1/users/auth/signin

- Input
{
 "email": STRING,     // any string e.g:  testing
 "passowrd": STRING,  // any string e.g:  testing
}
```

#### User Signout

```javascript

POST /api/v1/users/auth/signout

 No input // clear set JWT cookie

```

#### Create Account

```javascript

POST /api/v1/accounts/    // requires JWT authorization

- Input
{
 "accountType": STRING,   // saving, current, fixed-deposit
}
```

#### Credit Account

```javascript

POST  api/v1/transactions/credit // requires JWT authorization

- Input
{
 "accountNumber": STRING,       // retrieved from logged in user token
 "amount": FLOAT,               // e.g: 100.00
 "transactionType": STRING
}
```

#### Debit Account

```javascript

POST  api/v1/transactions/debit // requires JWT authorization

- Input
{
 "accountNumber": STRING,       // retrieved from logged in user token
 "amount": FLOAT,               // e.g: 100.00
 "transactionType": STRING      // credit, debit
}
```

## Author

[Tobechukwu Obitube](https://www.linkedin.com/in/tohbay/)

### NB:

This project is still ongoing.
Your kind reviews are highly appreciated.
