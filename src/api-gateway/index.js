import express from "express";
import path from "path";
import userAPI from "../services/Users";
import accountAPI from "../services/Accounts";
import transactionAPI from "../services/Transactions";
const router = express.Router();

// router.get("/", (req, res) => {
//   res.status(200).json({
//     status: true,
//     message: "Welcome to Tranzact API 👍🏼 🌍 ⚡️ 🥂 🏆",
//   });
// });

router.use("/users", userAPI);
router.use("/accounts", accountAPI);
router.use("/transactions", transactionAPI);

export default router;
