const config = {
  development: {
    username: process.env.POSTGRES_DEV_USER,
    password: process.env.POSTGRES_DEV_PASS,
    database: process.env.POSTGRES_DEV_DB,
    host: process.env.POSTGRES_DEV_HOST,
    dialect: "postgres",
    ssl: true,
    logging: true,
  },
  test: {
    username: process.env.POSTGRES_TEST_USER,
    password: process.env.POSTGRES_TEST_PASS,
    database: process.env.POSTGRES_TEST_DB,
    host: process.env.POSTGRES_TEST_HOST,
    dialect: "postgres",
    ssl: true,
    logging: true,
  },
  production: {
    username: process.env.POSTGRES_PROD_USER,
    password: process.env.POSTGRES_PROD_PASS,
    database: process.env.POSTGRES_PROD_DB,
    host: process.env.POSTGRES_PROD_HOST,
    dialect: "postgres",
  },
};

export default config[process.env.NODE_ENV];
