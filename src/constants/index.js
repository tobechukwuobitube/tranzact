export const signupConstraints = {
  username: {
    presence: true,
    length: { minimum: 8, maximum: 20 },
  },
  email: {
    presence: true,
    email: true,
  },
  password: {
    presence: true,
    length: { minimum: 8, maximum: 20 },
  },
};
export const loginConstraints = {
  email: {
    presence: true,
    email: true,
  },
  password: {
    presence: true,
    length: { minimum: 8, maximum: 20 },
  },
};

export const createAccountConstraints = {
  accountType: {
    inclusion: {
      within: ["savings", "current", "fixed-deposit"],
      message: "Account type can only be saving, current or fixed-deposit",
      presence: true,
    },
  },
};

export const creditAccountConstraints = {
  amount: {
    presence: true,
  },
  accountNumber: {
    presence: true,
  },
  transactionType: {
    inclusion: {
      within: ["credit", "debit"],
      message: "Transaction type can only be credit or debit",
      presence: true,
    },
  },
};

export const debitAccountConstraints = {
  amount: {
    presence: true,
  },
  accountNumber: {
    presence: true,
  },
  transactionType: {
    inclusion: {
      within: ["credit", "debit"],
      message: "Transaction type can only be credit or debit",
      presence: true,
    },
  },
};

export const fetchTransactions = {
  accountNumber: {
    presence: true,
  },
};
