"use strict";

module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "account",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
        required: true,
      },
      accountNumber: {
        type: DataTypes.UUID,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
        required: true,
      },
      accountType: {
        type: DataTypes.ENUM("savings", "current", "fixed-deposit"),
        allowNull: false,
        required: true,
      },
      status: {
        type: DataTypes.ENUM("dormant", "active"),
        allowNull: false,
        required: true,
        defaultValue: "dormant",
      },
      balance: {
        type: DataTypes.FLOAT,
        allowNull: false,
        required: true,
        defaultValue: 0,
      },

      // fk in User table
      userId: {
        type: DataTypes.UUID,
        require: true,
        allowNull: false,
      },
    },
    {
      paranoid: true,
    }
  );
};
