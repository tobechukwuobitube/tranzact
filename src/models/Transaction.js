"use strict";

module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "transaction",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      amount: {
        type: DataTypes.FLOAT,
        allowNull: false,
        required: true,
      },
      transactionType: {
        type: DataTypes.ENUM("credit", "debit"),
        allowNull: false,
        required: true,
      },
      previousBalance: {
        type: DataTypes.FLOAT,
        allowNull: false,
        required: true,
      },
      currentBalance: {
        type: DataTypes.FLOAT,
        allowNull: false,
        required: true,
      },
      // fk in Account table
      accountNumberId: {
        type: DataTypes.UUID,
        require: true,
        allowNull: false,
      },
    },
    {
      paranoid: true,
    }
  );
};
