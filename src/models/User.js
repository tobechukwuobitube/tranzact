export default (sequelize, DataTypes) => {
  return sequelize.define(
    "user",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      username: {
        type: DataTypes.STRING,
        isAlphanumeric: true,
        required: true,
        allowNull: false,
        len: [8, 20],
      },
      email: {
        type: DataTypes.STRING,
        required: true,
        allowNull: false,
        len: [7, 100],
        isEmail: true,
      },
      password: {
        type: DataTypes.STRING,
        required: true,
        allowNull: false,
      },
    },
    {
      paranoid: true,
    }
  );
};
