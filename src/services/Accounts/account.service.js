import "regenerator-runtime/runtime";

import db from "../../models";
import asyncHandler from "../../Helpers/asyncHandler";
import validate from "validate.js";
import { createAccountConstraints } from "../../constants";

const openAccount = asyncHandler(async (req, res) => {
  const { accountType } = req.body;

  const validation = validate({ accountType }, createAccountConstraints);
  if (validation) return res.status(400).json({ error: validation });

  const { userId } = req;

  const user = await db.user.findOne({ where: { id: userId } });
  if (!user)
    return res.status(400).json({
      error: true,
      message: "User not found",
    });

  try {
    const serviceOperation = await db.sequelize.transaction(
      async (transaction) => {
        const newAccount = await db.account.create(
          { accountType, userId },
          {
            transaction: transaction,
          }
        );
        return res.status(201).json({
          success: true,
          message: "Account successfully created",
          data: newAccount,
        });
      }
    );
  } catch (error) {
    return res.status(500).json({
      error: true,
      message: "Ooops, Something went wrong. Please contact admin for support",
    });
  }
});

export { openAccount };
