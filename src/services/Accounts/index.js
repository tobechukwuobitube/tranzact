import express from "express";
import { authorization } from "../../Helpers/JWT";
import {

  openAccount,

} from "./account.service";

const router = express.Router();

router.post("/", authorization, openAccount);


export default router;
