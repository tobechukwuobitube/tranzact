import express from "express";
import { authorization } from "../../Helpers/JWT";

import {
  creditTransaction,
  debitTransaction,
  getAllTransactionsPerAccount,
} from "./transaction.service";

const router = express.Router();

router.put("/credit", authorization, creditTransaction);
router.put("/debit", authorization, debitTransaction);
router.get("/:accountNumber", authorization, getAllTransactionsPerAccount);

export default router;
