import "regenerator-runtime/runtime";
import { Op } from "sequelize";
import db from "../../models";
import asyncHandler from "../../Helpers/asyncHandler";
import validate from "validate.js";
import {
  creditAccountConstraints,
  debitAccountConstraints,
  fetchTransactions,
} from "../../constants";

const creditTransaction = asyncHandler(async (req, res) => {
  const { amount, transactionType, accountNumber } = req.body;

  const validation = validate(
    { amount, transactionType, accountNumber },
    creditAccountConstraints
  );

  if (validation) return res.status(400).json({ error: validation });

  const accountToCredit = await db.account.findOne({
    where: { accountNumber: accountNumber },
  });

  if (!accountToCredit)
    return res.status(400).json({
      error: true,
      message:
        "Please provide a valid account to proceed. You can contact Admin for support",
    });

  const accountPreviousBalance = parseFloat(accountToCredit.balance);

  const transactionBalance =
    parseFloat(accountToCredit.balance) + parseFloat(amount);

  accountToCredit.balance = transactionBalance;
  await accountToCredit.save();

  try {
    const serviceOperation = await db.sequelize.transaction(
      async (transaction) => {
        const newAccountCredit = await db.transaction.create(
          {
            amount,
            accountNumberId: accountNumber,
            transactionType,
            previousBalance: accountPreviousBalance,
            currentBalance: transactionBalance,
          },
          {
            transaction: transaction,
          }
        );
        return res.status(201).json({
          success: true,
          message: "Transaction successfully credited",
          data: newAccountCredit,
        });
      }
    );
  } catch (error) {
    return res.status(500).json({
      error: true,
      message: "Ooops, Something went wrong. Please contact admin for support",
    });
  }
});

const debitTransaction = asyncHandler(async (req, res) => {
  const { amount, transactionType, accountNumber } = req.body;

  const validation = validate(
    { amount, transactionType, accountNumber },
    debitAccountConstraints
  );

  if (validation) return res.status(400).json({ error: validation });

  const accountToDebit = await db.account.findOne({
    where: { accountNumber: accountNumber },
  });

  if (!accountToDebit)
    return res.status(400).json({
      error: true,
      message:
        "Please provide a valid account to proceed. You can contact Admin for support",
    });

  if (accountToDebit.balance < amount)
    return res
      .status(400)
      .json({ error: true, message: "Insufficient balance" });
  const accountPreviousBalance = parseFloat(accountToDebit.balance);

  const transactionBalance =
    parseFloat(accountToDebit.balance) - parseFloat(amount);

  accountToDebit.balance = transactionBalance;
  await accountToDebit.save();

  try {
    const serviceOperation = await db.sequelize.transaction(
      async (transaction) => {
        const newAccountdebit = await db.transaction.create(
          {
            amount,
            accountNumberId: accountNumber,
            transactionType,
            previousBalance: accountPreviousBalance,
            currentBalance: transactionBalance,
          },
          {
            transaction: transaction,
          }
        );
        return res.status(201).json({
          success: true,
          message: "Transaction successfully debited",
          data: newAccountdebit,
        });
      }
    );
  } catch (error) {
    return res.status(500).json({
      error: true,
      message: "Ooops, Something went wrong. Please contact admin for support",
    });
  }
});

const getAllTransactionsPerAccount = asyncHandler(async (req, res) => {
  const { accountNumber } = req.params || req.body;

  const validation = validate({ accountNumber }, fetchTransactions);
  if (validation) return res.status(400).json({ error: validation });

  try {
    const serviceOperation = await db.sequelize.transaction(
      async (transaction) => {
        const accountNumberToFetchTransactions = await db.account.findOne(
          {
            where: { accountNumber: accountNumber },
          },
          {
            transaction: transaction,
          }
        );

        if (!accountNumberToFetchTransactions)
          return res.status(400).json({
            error: true,
            message:
              "Please provide a valid account to proceed. You can contact Admin for support",
          });

        const fetchedAccountTransactions = await db.transaction.findAll(
          {
            where: {
              accountNumberId: {
                [Op.eq]: accountNumberToFetchTransactions.accountNumber,
              },
            },
          },
          {
            transaction: transaction,
          }
        );

        if (!fetchedAccountTransactions.length === 0)
          return res.status(404).json({
            error: true,
            message: "No transaction records for this account",
          });

        return res.status(200).json({
          success: true,
          message: "Transactions successfully retrieved",
          data: fetchedAccountTransactions,
        });
      }
    );
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      error: true,
      message: "Ooops, Something went wrong. Please contact admin for support",
    });
  }
});

export { creditTransaction, debitTransaction, getAllTransactionsPerAccount };
