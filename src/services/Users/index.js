import express from "express";
import { userSignup, userSignin, userSignout } from "./user.service";

const router = express.Router();

router.post("/auth/signup", userSignup);
router.post("/auth/signin", userSignin);
router.post("/auth/signout", userSignout);

export default router;
