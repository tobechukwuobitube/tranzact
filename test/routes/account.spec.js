import request from "supertest";
import { expect } from "chai";

import app from "../../src/app";
import db from "../../src/models";
import { createTokens } from "../../src/Helpers/JWT";

beforeEach(async () => {
  await db.account.destroy({ cascade: true, truncate: true });
});

describe("Test Account", () => {
  it("returns 201 ok when account is successfully created", async () => {
    const user = await signUpValidUser();
    await signInValidUser();
    const userList = await db.user.findAll();
    const savedUser = userList[0];

    const newuser = await db.user.findOne({
      where: { id: savedUser.id },
    });

    const accessToken = createTokens(newuser);

    const response = await request(app)
      .post("/api/v1/accounts")
      .set("Cookie", [`access-token=${accessToken}`])
      .send({
        accountType: "savings",
        userId: `${newuser.id}`,
      });

    await DestroyDummyUser(user);

    expect(response.status).to.eql(201);
  });
});

const validUser = {
  username: "test1234",
  email: "test@test.com",
  password: "test1234",
};

const validLoginCredentials = {
  email: "test@test.com",
  password: "test1234",
};

const signInValidUser = async () => {
  return await request(app)
    .post("/api/v1/users/auth/login")
    .send(validLoginCredentials);
};

const signUpValidUser = async () => {
  return await request(app).post("/api/v1/users/auth/signup").send(validUser);
};

async function DestroyDummyUser(user) {
  return await user.destroy({ force: true });
}
