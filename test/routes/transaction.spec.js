import request from "supertest";
import { expect } from "chai";

import app from "../../src/app";
import db from "../../src/models";
import { createTokens } from "../../src/Helpers/JWT";

beforeEach(async () => {
  await db.account.destroy({ cascade: true, truncate: true });
});

describe("Test Transaction", () => {
  it("returns 201 ok when a account is successfully credited", async () => {
    const user = await signUpValidUser();
    await signInValidUser();
    const userList = await db.user.findAll();
    const savedUser = userList[0];

    const newUser = await db.user.findOne({
      where: { id: savedUser.id },
    });

    const accessToken = createTokens(newUser);

    const newAccount = await request(app)
      .post("/api/v1/accounts")
      .set("Cookie", [`access-token=${accessToken}`])
      .send({
        accountType: "savings",
        userId: `${newUser.id}`,
      });

    const foundAccount = await db.account.findOne({
      where: { accountNumber: newAccount.body.data.accountNumber },
    });

    const creditAmount = 100;

    const transactionBalance =
      parseFloat(foundAccount.balance) + parseFloat(creditAmount);

    const creditTransaction = {
      amount: creditAmount,
      accountNumber: foundAccount.accountNumber,
      accountNumberId: foundAccount.accountNumber,
      transactionType: "credit",
      previousBalance: parseFloat(foundAccount.balance),
      currentBalance: transactionBalance,
    };

    foundAccount.balance = transactionBalance;
    await foundAccount.save();

    const response = await request(app)
      .put(`/api/v1/transactions/credit`)
      .set("Cookie", [`access-token=${accessToken}`])
      .send(creditTransaction);

    await DestroyDummyUser(user);

    expect(response.status).to.eql(201);
  });

  it("returns 201 ok when a account is successfully debited", async () => {
    const user = await signUpValidUser();
    await signInValidUser();
    const userList = await db.user.findAll();
    const savedUser = userList[0];

    const newUser = await db.user.findOne({
      where: { id: savedUser.id },
    });

    const accessToken = createTokens(newUser);

    const newAccount = await request(app)
      .post("/api/v1/accounts")
      .set("Cookie", [`access-token=${accessToken}`])
      .send({
        accountType: "savings",
        userId: `${newUser.id}`,
      });

    let foundAccount = await db.account.findOne({
      where: { accountNumber: newAccount.body.data.accountNumber },
    });

    const creditAmount = 100;

    let transactionBalance =
      parseFloat(foundAccount.balance) + parseFloat(creditAmount);

    const creditTransaction = {
      amount: creditAmount,
      accountNumber: foundAccount.accountNumber,
      accountNumberId: foundAccount.accountNumber,
      transactionType: "credit",
      previousBalance: parseFloat(foundAccount.balance),
      currentBalance: transactionBalance,
    };

    foundAccount.balance = transactionBalance;
    await foundAccount.save();

    await request(app)
      .put(`/api/v1/transactions/credit`)
      .set("Cookie", [`access-token=${accessToken}`])
      .send(creditTransaction);

    foundAccount = await db.account.findOne({
      where: { accountNumber: newAccount.body.data.accountNumber },
    });

    const debitAmount = 100;

    transactionBalance =
      parseFloat(foundAccount.balance) - parseFloat(debitAmount);

    foundAccount.balance = transactionBalance;
    await foundAccount.save();

    const debitTransaction = {
      amount: debitAmount,
      accountNumber: foundAccount.accountNumber,
      accountNumberId: foundAccount.accountNumber,
      transactionType: "debit",
      previousBalance: parseFloat(foundAccount.balance),
      currentBalance: transactionBalance,
    };

    const response = await request(app)
      .put(`/api/v1/transactions/debit`)
      .set("Cookie", [`access-token=${accessToken}`])
      .send(debitTransaction);

    await DestroyDummyUser(user);

    expect(response.status).to.eql(201);
  });

  it("returns 200 ok when a list of transactions (ledger) is pulled for a specific account number", async () => {
    const user = await signUpValidUser();
    await signInValidUser();
    const userList = await db.user.findAll();
    const savedUser = userList[0];

    const newUser = await db.user.findOne({
      where: { id: savedUser.id },
    });

    const accessToken = createTokens(newUser);

    const newAccount = await request(app)
      .post("/api/v1/accounts")
      .set("Cookie", [`access-token=${accessToken}`])
      .send({
        accountType: "savings",
        userId: `${newUser.id}`,
      });

    let foundAccount = await db.account.findOne({
      where: { accountNumber: newAccount.body.data.accountNumber },
    });

    const creditAmount = 100;

    let transactionBalance =
      parseFloat(foundAccount.balance) + parseFloat(creditAmount);

    const creditTransaction = {
      amount: creditAmount,
      accountNumber: foundAccount.accountNumber,
      accountNumberId: foundAccount.accountNumber,
      transactionType: "credit",
      previousBalance: parseFloat(foundAccount.balance),
      currentBalance: transactionBalance,
    };

    foundAccount.balance = transactionBalance;
    await foundAccount.save();

    await request(app)
      .put(`/api/v1/transactions/credit`)
      .set("Cookie", [`access-token=${accessToken}`])
      .send(creditTransaction);

    await request(app)
      .put(`/api/v1/transactions/credit`)
      .set("Cookie", [`access-token=${accessToken}`])
      .send(creditTransaction);

    foundAccount = await db.account.findOne({
      where: { accountNumber: newAccount.body.data.accountNumber },
    });

    const debitAmount = 100;

    transactionBalance =
      parseFloat(foundAccount.balance) - parseFloat(debitAmount);

    foundAccount.balance = transactionBalance;
    await foundAccount.save();

    const debitTransaction = {
      amount: debitAmount,
      accountNumber: foundAccount.accountNumber,
      accountNumberId: foundAccount.accountNumber,
      transactionType: "debit",
      previousBalance: parseFloat(foundAccount.balance),
      currentBalance: transactionBalance,
    };

    await request(app)
      .put(`/api/v1/transactions/debit`)
      .set("Cookie", [`access-token=${accessToken}`])
      .send(debitTransaction);

    const fetchedtransactions = await db.transaction.findAll({
      where: {
        accountNumberId: foundAccount.accountNumber,
      },
    });

    const response = await request(app)
      .get(`/api/v1/transactions/${foundAccount.accountNumber}`)
      .set("Cookie", [`access-token=${accessToken}`]);

    await DestroyDummyUser(user);

    expect(response.body.data.length > 2).to.eql(true);
    expect(response.body.data).to.be.an("array");
  });
});

const validUser = {
  username: "test1234",
  email: "test@test.com",
  password: "test1234",
};

const validLoginCredentials = {
  email: "test@test.com",
  password: "test1234",
};

const signInValidUser = async () => {
  return await request(app)
    .post("/api/v1/users/auth/login")
    .send(validLoginCredentials);
};

const signUpValidUser = async () => {
  return await request(app).post("/api/v1/users/auth/signup").send(validUser);
};

async function DestroyDummyUser(user) {
  return await user.destroy({ force: true });
}
